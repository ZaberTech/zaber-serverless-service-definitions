import { JSONSchema7 } from 'json-schema';

export const PeripheralProvisioningParamsSchema: JSONSchema7 = {
  $comment: 'Peripheral provisioning parameters of the virtual device creation endpoint.',
  type: 'object',
  properties: {
    peripheralId: {
      type: 'integer',
      $comment: 'Peripheral ID to set on one axis of a new virtual controller.',
    },
  },
  required: ['peripheralId'],
  additionalProperties: false,
};


export interface PeripheralProvisioningParams {
  peripheralId: number;
}


export const DeviceProvisioningParamsSchema: JSONSchema7 = {
  $comment: 'Device provisioning parameters of the virtual device creation endpoint.',
  type: 'object',
  properties: {
    deviceId: {
      type: 'integer',
      $comment: 'Device ID to set on one new virtual device.',
    },
    peripherals: {
      type: 'array',
      items: PeripheralProvisioningParamsSchema,
      $comment: 'Ordered list of peripheral IDs to create on one Virtual Device.',
    },
  },
  required: ['deviceId'],
  additionalProperties: false,
};


export interface DeviceProvisioningParams {
  deviceId: number;
  peripherals?: PeripheralProvisioningParams[];
}


export const CreateVirtualDeviceChainParamsSchema: JSONSchema7 = {
  $comment: 'Input parameters for the virtual device creation endpoint.',
  type: 'object',
  properties: {
    name: {
      type: 'string',
      $comment: 'Optional user-assigned label for the Virtual Device chain.',
    },
    devices: {
      type: 'array',
      items: DeviceProvisioningParamsSchema,
      $comment: 'Ordered list of device and peripheral IDs to create in the Virtual Device chain.',
    },
    description: {
      type: 'string',
      $comment: 'Optional user-assigned description of the Virtual Device chain.',
    },
    createdBy: {
      type: 'string',
      $comment: 'Used to identify the software that created the Virtual Device. May not be set if the user called the API themselves.',
    },
    logDebugOverride: {
      type: 'string',
      $comment: 'Optional override for the simulation manager\'s logging message mask (npm debug format).',
    },
  },
  required: ['devices'],
  additionalProperties: false,
};


export interface CreateVirtualDeviceChainParams {
  name?: string;
  devices: DeviceProvisioningParams[];
  description?: string;
  createdBy?: string;
  logDebugOverride?: string;
}


export const VirtualDevicePowerSwitchParamsSchema: JSONSchema7 = {
  $comment: 'Parameters for the virtual device on/off switch endpoint.',
  type: 'object',
  properties: {
    cloudId: {
      type: 'string',
      $comment: 'Cloud ID of the device chain to turn on or off.',
    },
    power: {
      type: 'boolean',
      $comment: 'New state of the power switch - true to turn on, false to turn off.',
    },
    startedBy: {
      type: 'string',
      $comment: 'Used to identify the software that started the Virtual Device. May not be set if the user called the API themselves.',
    },
    logDebugOverride: {
      type: 'string',
      $comment: 'Optional override for the simulation manager logging message mask (npm debug format).',
    },
  },
  required: ['cloudId', 'power'],
  additionalProperties: false,
};


export interface VirtualDevicePowerSwitchParams {
  cloudId: string;
  power: boolean;
  startedBy?: string;
  logDebugOverride?: string;
}


export const VirtualDeviceRenameChainParamsSchema: JSONSchema7 = {
  $comment: 'Parameters for renaming a Virtual Device chain.',
  type: 'object',
  properties: {
    cloudId: {
      type: 'string',
      $comment: 'Cloud ID of the device chain to rename.',
    },
    name: {
      type: 'string',
      $comment: 'New user-assigned label for the Virtual Device chain.',
    },
  },
  required: ['cloudId', 'name'],
  additionalProperties: false,
};


export interface VirtualDeviceRenameChainParams {
  cloudId: string;
  name: string;
}
