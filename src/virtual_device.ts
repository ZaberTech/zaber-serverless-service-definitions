import { DeviceProvisioningParams } from './schema';


/**
 * States that a virtual device simulation can be in.
 */
export enum SimulationState {
  Creating = 'creating',
  TurningOn = 'turning on',
  On = 'on',
  Off = 'off',
  TurningOff = 'turning off',
  Deleting = 'deleting',
  Deleted = 'deleted',
  Error = 'error',
}


/**
 * Return type for several virtual device endpoints.
 */
export interface VirtualDeviceChainState {
  cloudId: string;
  state: SimulationState;
}


/**
 * Return type for the list my virtual devices endpoint.
 */
export interface VirtualDeviceChainProperties {
  cloudId: string;
  name: string;
  state: SimulationState;
  error?: string;
  devices: DeviceProvisioningParams[];
  expires?: string;
}


/**
 * Data type used by the Simulation Manager to store new device states in
 * the back end.
 */
export interface VirtualDeviceStateUpdate {
  realm: string;
  cloudId: string;
  state: SimulationState;
  error?: string;
}


/**
 * Data type used for the keepalive heartbeat.
 */
export interface VirtualDeviceHeartbeat {
  cloudId: string;
}


/**
 * Return type for virtual device existence check.
 */
export interface VirtualDeviceExistence {
  cloudId: string;
  exists: boolean;
  public: boolean;  // Only consider valid if exists = true.
  expires?: string; // Only valid if exists = true and public = true.
}
