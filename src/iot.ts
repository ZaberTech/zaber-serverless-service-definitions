/**
 * Return value from the IoT token endpoint.
 */
export interface IotTokenApiResult {
  url: string;
  defaultRealm: string;
}
